<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class PhpCaptchaController extends Controller {
	private static $session_id = 'php_captcha';
	private static $width = 200; // max 500
	private static $height = 35; // max 200
	private static $num_chars = 5;
	private static $num_lines = 70;
	private static $char_shadow = false;
	private static $owner_text = '';
	private static $char_set = '';
	private static $case_insensitive = true;
	private static $background = 'phpcaptcha/images/bg1.jpeg';
	private static $min_font_size = 16;
	private static $max_font_size = 25;
	private static $use_color = true;
	private static $file_type = 'jpeg';
	
	/**
     * Use this singelton to applay you custon configured PhpCaptcha class.
     * If this will be left unset the fields constructor will initialise
     * it with a defaul PhpCaptcha class.
     *
     * @var PhpCaptcha
     */
    private static $prototype;
	
	private static $allowed_actions = array(
		'genimage'
	);

    static function get_php_captcha() {
		$config = singleton('PhpCaptchaController')->config();
		if(!($p = $config->get('prototype'))) {
			$f = array(
				Director::baseFolder().'/phpcaptcha/fonts/VeraIt.ttf',
				Director::baseFolder().'/phpcaptcha/fonts/VeraMono.ttf',
				Director::baseFolder().'/phpcaptcha/fonts/VeraSe.ttf',
				Director::baseFolder().'/phpcaptcha/fonts/VeraSeBd.ttf'
			);
			$p = new PhpCaptcha($f, $config->get('width'), $config->get('height'));
			$p->SetNumChars($config->get('num_chars'));
         	$p->SetNumLines($config->get('num_lines'));
         	$p->DisplayShadow($config->get('char_shadow'));
         	$p->SetOwnerText($config->get('owner_text'));
         	$p->SetCharSet($config->get('char_set'));
         	$p->CaseInsensitive($config->get('case_insensitive'));
         	$p->SetBackgroundImages(Director::baseFolder()."/".$config->get('background'));
         	$p->SetMinFontSize($config->get('min_font_size'));
         	$p->SetMaxFontSize($config->get('max_font_size'));
         	$p->UseColour($config->get('use_color'));
         	$p->SetFileType($config->get('file_type'));
			
			$config->prototype = $p;
		}

		return $p;
    }

    function genimage() {
	    PhpCaptchaController::get_php_captcha()->Create();
    }
}

?>
