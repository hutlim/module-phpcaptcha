<?php
/**
 * Provides an {@link FormField} which allows form to validate for non-bot submissions
 * by giving them a challenge to decrypt an image.
 *
 * @module phpcaptcha
 */
class PhpCaptchaField extends TextField {
	public function Type(){
		return 'phpcaptcha text';
	}

	public function Title() {
		return _t('PhpCaptchaField.CAPTCHA', "Captcha");
	}
		
	public function Field($properties = array()) {
		return '<img src="' . Controller::join_links('PhpCaptchaController', 'genimage') . '" alt="captcha" /><br />' . parent::Field($properties);
	}
	
	/**
	 * Validate the PhpCaptcha code
	 *
	 * @param Validator $validator
	 * @return boolean
	 */
	public function validate($validator) {
		if(!PhpCaptcha::Validate($this->dataValue())) {
			$validator->validationError(
				$this->getName(),
				_t(
					'PhpCaptchaField.WRONG',
					"The text you entered doesn't match the captcha. Please try again"
				),
				"validation",
				false
			);
			return false;
		}
		else {
			return true;
		}
	}
}