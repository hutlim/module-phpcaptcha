<?php

/**
 * Protecter class to handle spam protection interface 
 *
 * @package recaptcha
 */

class PhpCaptchaProtector implements SpamProtector {
	
	/**
	 * Return the Field that we will use in this protector
	 * 
	 * @return string
	 */
	function getFormField($name = "PhpCaptchaField", $title = "Captcha", $value = null) {
		return new PhpCaptchaField($name, $title, $value);
	}
	
	/**
	 * Not used by MathSpamProtector
	 */
	public function setFieldMapping($fieldMapping) {}
}
